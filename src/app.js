// app.js
//全局导入封装的request
import './utils/http'
// 全局导入封装的小弹框
import './utils/utils'

App({
  token: '',
  // 应用钩子函数,刚启用小程序就会执行，只执行一次，获取token
  onLaunch() {
    this.getToken()
  },
  // 封装获取存储本地的token
  getToken() {
    // 异步获取
    wx.getStorage({
      key: '存token',
      // result是获取的本地token值
      success: (result) => {
        console.log(result);
        this.token = result.data
        return this.token
      },

    });

  },
  // 封装本地存token
  setToken(a) {
    wx.setStorageSync("存token", 'Bearer ' + a);
  },
  // globalData: {}
})
