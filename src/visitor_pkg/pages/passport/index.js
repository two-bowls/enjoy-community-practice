Page({
  data: {

  },
  // 获取通行证页面数据
  onLoad({ id }) {

    this.getList(id)
  },
  async getList(a) {
    const { data } = await wx.http.get('/visitor/' + a)
    console.log(data, 'data');
    this.setData({
      ...data
    })
  },
  // 分享给朋友
  onShareAppMessage() {
    return {
      title: '查看通行证',
      path: '/visitor_pkg/pages/passport/index',
      imageUrl: 'https://enjoy-plus.oss-cn-beijing.aliyuncs.com/images/share_poster.png',
    }
  },
  // 保存到本地
  async save() {
    // 1.注：saveImageToPhotosAlbum参数是临时地址，这里获取临时地址
    const { path } = await wx.getImageInfo({ src: this.data.url })
    // console.log(path, 'path');
    // 2.存本地
    wx.saveImageToPhotosAlbum({
      filePath: path,
      success(res) {
        console.log(res, '存成');
      }
    })
  }
})
