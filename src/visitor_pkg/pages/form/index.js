Page({
  data: {
    dateLayerVisible: false,
    houseLayerVisible: false,
    houseList: [
      // { name: '北京西三旗花园1号楼 101' },
      // { name: '北京东村家园3号楼 302' },
      // { name: '北京育新花园3号楼 703' },
      // { name: '北京天通苑北苑8号楼 403' },
    ],
    // 选中的房屋
    houseName: '',
    // 房间id
    houseId: '',
    // 拜访人姓名
    name: '',
    // 拜访人性别number型
    gender: 0,
    // 手机号
    mobile: '',
    // 拜访时间
    visitDate: '',
    // 当前时间戳
    nowTime: +new Date()
  },
  onLoad() {
    this.getHouseList()
  },
  // 封装获得渲染房屋弹框数据
  async getHouseList() {
    // 请求数据接口，结构data为数组
    const { data } = await wx.http.get('/house')
    console.log(data, '房屋列表');
    this.setData({
      houseList: JSON.parse(JSON.stringify(data))
    })
  },
  // 弹框选中房屋
  onSelectHouse({ detail: { id, name } }) {
    // console.log(detail, 'ev');
    this.setData({
      houseName: name,
      houseId: id
    })
  },
  // 弹框时间选择
  setDate({ detail }) {
    console.log(detail, '时间');
    this.setData({
      dateLayerVisible: false,
      visitDate: wx.utils.dayjsDate(detail)
    })
  },
  // 时间弹窗关闭
  off() {
    this.setData({
      dateLayerVisible: false
    })
  },
  openHouseLayer() {
    this.setData({ houseLayerVisible: true })
  },
  closeHouseLayer() {
    this.setData({ houseLayerVisible: false })
  },
  // 点击是打开弹窗
  openDateLayer() {
    this.setData({ dateLayerVisible: true })
  },
  closeDateLayer() {
    this.setData({ dateLayerVisible: false })
  },
  // 点击提交
  async goPassport() {
    // 校验
    const { houseId, name, gender, mobile, visitDate } = this.data
    if (!houseId) return wx.utils.toast('请选择房屋', 'error')
    if (!/^[\u4e00-\u9fa5]{2,5}$/.test(name)) return wx.utils.toast('填写姓名', 'error')
    if (!gender) return wx.utils.toast('请选择性别', 'error')
    if (!/^[1][3-8][0-9]{9}$/.test(mobile)) return wx.utils.toast('填写手机号', 'error')
    if (!visitDate) return wx.utils.toast('填拜访时间', 'error')
    // 提交数据
    try {
      const { data } = await wx.http.post('/visitor', { houseId, name, gender, mobile, visitDate })
      console.log(data, 'res');
      setTimeout(() => {
        wx.utils.toast('提交拜访成功', 'success')
      }, 2000)
      // 跳转，携带访客请求回来的id
      wx.navigateTo({
        url: '/visitor_pkg/pages/passport/index?id=' + data.id,
      })
    }
    catch (error) {
      wx.utils.toast('提交拜访失败')
      console.log(error, '提交拜访失败');
    }

  },
})
