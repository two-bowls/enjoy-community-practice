Page({
  data: {
    list: []
  },
  onLoad() {
    this.getList()
  },
  // 封装获取数据
  async getList() {
    const data = await wx.http.get('/visitor')
    console.log(data.data.rows, 'res');
    this.setData({
      list: JSON.parse(JSON.stringify(data.data.rows))
    })
  },
  goPassport() {
    wx.navigateTo({
      url: '/visitor_pkg/pages/passport/index',
    })
  },
})
