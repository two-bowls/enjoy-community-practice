// components/auth/auth.js
// ------------组件监测是否有token，根据这个决定是否显示组件插槽包裹的内容，如果没有token返回登录页面，并且携带当前页面路径，用来登录后直接登录
Component({
  /**
   * 组件的属性列表
   */
  properties: {

  },

  /**
   * 组件的初始数据
   */
  data: {
    // 是否展示组件内的内容
    isShow: true
  },
  // 组件生命周期
  lifetimes: {
    attached: function () {
      console.log(getApp().token, 'getApp().token');
      const token = getApp().token
      // 根据是否有token来决定内容是否展示
      this.setData({
        isShow: token ? true : false
      })
      this.istoken()
    }
  },
  /**
   * 组件的方法列表
   */
  methods: {
    // 携带当前路径
    istoken() {
      // 获取当前路劲,获取的是数组，数组成员最后一个是当前路径
      const pages = getCurrentPages();
      console.log(pages, 'pages');
      if (!getApp().token) {
        wx.redirectTo({
          url: `/pages/login/index?url=/${pages[pages.length - 1].route}`
        });

      }
    }
  }
})
