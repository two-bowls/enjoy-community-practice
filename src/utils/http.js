// 封装请求，挂载在wx上，可以用promise形式,使用的是第三方包从根本进行第二次封装
import request from "wechat-http";
// 基础地址
request.baseURL = "https://live-api.itheima.net"
// 1.响应拦截器
// 得到的结果先解构为data一遍再返回给请求页面
request.intercept.response = ({ data, statusCode }) => {
  // 1.1如果返回的编码不是10000，请求错误,弹出小框提示错误内容并且返回给请求页面错误的结果
  if (data.code !== 1e4) {
    wx.utils.toast(data.message, 'error')
    // 1.2token失效情况，编码code不是10000为不正常，并且statusCode === 401为token失效
    if (statusCode === 401) {
      // token失效，返回登录页面重新登录携带当前路径为参数
      const pages = getCurrentPages();
      const getUrl = pages[pages.length - 1].route
      wx.redirectTo({
        url: '/pages/login/index?url=' + getUrl
      });
    }
    // 失败返回错误的
    return Promise.reject(data)
  }
  // 请求返回正常返回data
  return data
}
// 2.请求拦截器//拦截请求  params请求参数
request.intercept.request = (params) => {
  // 先检查有没有token
  const token = getApp().token;
  console.log(token, '拦截器');
  if (token) {
    // 先声明为对象
    params.header = {}
    // 再赋值给变量的属性名
    params.header.Authorization = token

  }
  // 注：！！！返回请求参数
  return params
}

// 挂载
wx.http = request
