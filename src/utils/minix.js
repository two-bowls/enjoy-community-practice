// eslint-disable-next-line no-undef
const minix = Behavior({
  data: {
    tabbarList: [
      "/pages/index/index",
      "/pages/my/index",
    ]
  },
  methods: {
    isTabbar(url) {
      const res = this.data.tabbarList.includes(url)
      return res
    }
  }
})
export default minix