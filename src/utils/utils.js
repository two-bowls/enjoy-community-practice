// 封装公共组件
// 封装dayjs，导入dayjs
import dayjs from 'dayjs'
const utils = {
  // --------提示小弹框
  toast(title = "数据加载失败...", icon = "none") {
    // wx.showToast是wx自带的方法，不需要导入第三方包
    wx.showToast({
      // 点击遮罩是否关闭
      mask: true,
      title: title,
      icon: icon,
    });
    // 2秒钟后关闭小弹窗
    setTimeout(() => {
      wx.hideToast()
    }, 2000)

  },
  // 传入一个时间或者是时间戳转换为指定格式
  dayjsDate(time, type = "YYYY-MM-DD") {
    return dayjs(time).format(type)
  }
}
// 挂载,挂载之后不需要导出，已经在wx上了，但是使用时还是需要默认导入到入口文件上
wx.utils = utils