// map.js
// repair_pkg/pages/detail/index.js
import qqMap from '../../../utils/qqMap'
Page({
  data: {
    latitude: 36.104587,
    longitude: 114.396407,
    markers: [
      // 起点：传智播客
      {
        id: 1,
        latitude: 36.102876,
        longitude: 114.393856,
        width: 24,
        height: 30,
        iconPath: '/static/images/marker.png',
      },
      // 终点：云趣园1区
      {
        id: 2,
        latitude: 36.104411,
        longitude: 114.398548,
        width: 24,
        height: 30,
      },
    ],
    status: ""
  },
  // ...
  onLoad({ id }) {
    // 获取维修详情
    this.getRepairDetail(id)
    // 如果状态是已经上门，路线显示
    if (this.data.status === 2) {
      this.createPl()
    }
    this.createPl()
  },
  async getRepairDetail(id) {
    // 请求数据接口
    const { code, data: repairDetail } = await wx.http.get('/repair/' + id)

    // 校验接口调用结果
    if (code !== 10000) return wx.utils.toast('获取报修详情失败!')
    // 渲染报修详情
    this.setData({ ...repairDetail })
  },
  // 规划路线
  createPl() {
    const { markers } = this.data
    // 使用的sdk实例化来的原型对象上的方法
    qqMap.direction({
      mode: 'driving',
      from: {
        latitude: markers[0].latitude,
        longitude: markers[0].longitude
      },
      to: {
        latitude: markers[1].latitude,
        longitude: markers[1].longitude
      },
      success: (res) => {
        console.log(res);
        const ret = res;
        const coors = ret.result.routes[0].polyline, pl = [];
        //坐标解压（返回的点串坐标，通过前向差分进行压缩）
        const kr = 1000000;
        for (let i = 2; i < coors.length; i++) {
          coors[i] = Number(coors[i - 2]) + Number(coors[i]) / kr;
        }
        //将解压后的坐标放入点串数组pl中
        for (let i = 0; i < coors.length; i += 2) {
          pl.push({ latitude: coors[i], longitude: coors[i + 1] })
        }
        console.log(pl)
        //设置polyline属性，将路线显示出来,将解压坐标第一个数据作为起点
        this.setData({
          latitude: pl[0].latitude,
          longitude: pl[0].longitude,
          polyline: [{
            points: pl,
            color: '#FF0000DD',
            width: 4
          }]
        })
      },
    })
  },
  // 点击取消
  async cancel() {
    wx.showModal({
      title: '确定',
      content: '亲，确定取消吗',
      success: async (result) => {
        if (result.confirm) {
          // wx.http.put('/cancel/repaire/' + this.data.id)
          const res = await wx.http.put(`/cancel/repaire/${this.data.id}`)
          console.log(res, '取消');
          // 不需要写这个，拦截器里已经写过了
          // if (res.code === 1e4) {
          //   wx.utils.toast('已取消', 'success')
          // }
        }
      }
    });
  },
  // 点击修改
  ediInfo() {
    // 跳转页面并把当前数据的id传过去，修改表单复用的添加页，id用来区分
    wx.navigateTo({
      url: '/repair_pkg/pages/form/index?id=' + this.data.id,
    });

  }
})
