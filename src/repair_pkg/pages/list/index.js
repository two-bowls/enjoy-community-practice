Page({
  data: {
    repairList: ""
  },
  // 每次展示
  onShow() {
    this.getLIst()
  },
  // 封装获取
  async getLIst() {
    const { code, data: { rows: repairList } } = await wx.http.get('/repair', { current: 1, pageSize: 10 })
    // 检测接口调用的结果
    if (code !== 10000) return wx.utils.toast('获取报修列表失败!')
    // 渲染报修列表
    this.setData({
      repairList
    })
  },
  goDetail({ mark: { id } }) {
    wx.navigateTo({
      url: '/repair_pkg/pages/detail/index?id=' + id,
    })
  },
  addRepair() {
    wx.navigateTo({
      url: '/repair_pkg/pages/form/index',
    })
  },
})
