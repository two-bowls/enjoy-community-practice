Page({
  data: {
    currentDate: new Date().getTime(),
    // 保修房屋弹框
    houseLayerVisible: false,
    // 选择维修项目弹框
    repairLayerVisible: false,
    // 时间选择框是否出现
    dateLayerVisible: false,
    // 弹框房屋选中的值
    houseValue: '',
    // 弹框维修项目选中的值
    Item: '',
    // 手机电话
    number: '',
    // 预约时间
    goTime: '',
    // 预约时间最早的上线,为时间戳自动转换为弹窗中的时间
    minDate: +new Date(),
    // 报修房屋渲染的列表
    houseList: [],
    // 房屋id
    houseId: '',
    // 问题描述
    description: '',
    // 维修项目列表
    repairItem: [],
    // 报修项目名字
    item: '',
    // 选中的维修项目id
    repairItemId: '',
    // 上传维修图片
    attachment: [
      // { url: '/repair_pkg/static/uploads/attachment.jpg' },
      // { url: '/repair_pkg/static/uploads/attachment.jpg' },
    ],
  },
  onLoad({ id }) {
    // 调用获取地址列表
    this.getMapList()
    // 调用获取维修项目列表
    this.getTimeList()
    if (id) {
      // 改页头
      wx.setNavigationBarTitle({
        title: '修改报修信息'
      });
      // 数据回填
      this.getRepairDetail(id)
    }
  },
  // 封装页面回填信息获取
  async getRepairDetail(id) {
    // 请求数据接口
    const { code, data: repairDetail } = await wx.http.get('/repair/' + id)
    // 检测接口调用结果
    if (code !== 10000) return wx.utils.toast('获取报修信息失败!')
    console.log(repairDetail, '回显');
    // 渲染报修信息
    this.setData({
      ...repairDetail,
      houseValue: repairDetail.houseInfo,
      item: repairDetail.repairItemName,
      number: repairDetail.mobile,
      goTime: repairDetail.appointment
    })
  },
  // 输入手机号双向绑定不生效
  handleInput({ detail }) {
    this.setData({
      number: detail
    })
  },
  // 弹框----报修房屋，获得点击的值
  onSelect({ detail: { name, id } }) {
    console.log(name, id, '报修');
    this.setData({
      houseValue: name,
      houseId: id,
    })
  },
  // 弹框---维修项目{ name, id }
  selectRepairItem({ detail: { name, id } }) {
    // console.log(name, id, '维修项目');
    this.setData({
      repairItemId: id,
      item: name
    })

  },
  // 封装获得维修项目列表
  async getTimeList() {
    const { data } = await wx.http.get("/repairItem")
    // console.log(data, '维修项目');
    this.setData({
      repairItem: data
    })
  },
  // 弹框--时间detail为时间戳
  onInput({ detail }) {
    // 选择数就会关闭预约时间弹框

    // console.log(detail, '时间');
    // 把时间戳变成时间格式
    this.setData({
      dateLayerVisible: false,
      // 调用方法将时间戳转换为指定格式
      goTime: wx.utils.dayjsDate(detail)
    })
  },
  // 获取选择报修弹框的渲染列表
  async getMapList() {
    const { data } = await wx.http.get('/house')
    // console.log(data, '地址列表');
    // const newList = data.forEach(item => {
    //   item.name = item.point
    // });
    this.setData({
      houseList: data,
    })
  },
  // ----上传图片
  afterRead({ detail }) {
    // 上传后台获取上期地址
    wx.uploadFile({
      url: wx.http.baseURL + '/upload',
      filePath: detail.file.url,
      name: 'file',
      header: {
        Authorization: getApp().token,
      },
      success: ({ data }) => {
        const JSONdata = JSON.parse(data)
        if (JSONdata.code !== 1e4) {
          wx.utils.toast('上传错误上传错误', 'error')
        }
        // 长期路径
        const { attachment } = this.data
        attachment.push(JSONdata.data)
        this.setData({
          attachment: attachment
        })
      }
    });

  },

  // 点击inpu框，出现弹框
  openHouseLayer() {
    this.setData({ houseLayerVisible: true })
  },
  closeHouseLayer() {
    this.setData({ houseLayerVisible: false })
  },
  openRepairLayer() {
    this.setData({ repairLayerVisible: true })
  },
  closeRepairLayer() {
    this.setData({
      repairLayerVisible: false,
    })
  },

  openDateLayer() {
    this.setData({ dateLayerVisible: true })
  },
  closeDateLayer() {
    this.setData({ dateLayerVisible: false })
  },
  // -----点击提交
  async goList() {
    // 校验-----这里结构id,如果是修改页面，会存入id到data里，如果是添加页面，id找不到undefind，请求时携带自动不会代入
    const { houseId, repairItemId, number: mobile, goTime: appointment, description, attachment, id } = this.data
    // console.log(houseId, repairItemId, mobile, appointment, description, attachment, 'houseId, repairItemId, mobile, goTime: appointment,description,attachment');
    if (!houseId) return wx.utils.toast('请选择房屋信息!')
    if (!repairItemId) return wx.utils.toast('请选择维修项目!')
    if (!/^[1][3-8][0-9]{9}$/.test(mobile.trim())) return wx.utils.toast('请填写正确的手机号码!')
    if (!appointment) return wx.utils.toast('请选择预约日期!')
    if (!description.trim()) return wx.utils.toast('请填写问题描述!')
    try {
      await wx.http.post("/repair", { houseId, repairItemId, mobile, appointment, description, attachment, id })
      // console.log(res);
      // 定时器，跳转过后弹窗
      setTimeout(() => {
        wx.utils.toast('提交成功', 'success')
      }, 2000)
    }
    catch (error) {
      wx.utils.toast("提交未成功", 'error')
    }
    wx.reLaunch({
      url: '/repair_pkg/pages/list/index',
    })
  },
})
