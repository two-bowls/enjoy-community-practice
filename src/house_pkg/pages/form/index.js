Page({
  data: {
    point: '北京西三旗花园',
    building: '1',
    room: '101',
    // 姓名
    name: '',
    // 性别
    six: 0,
    // 手机号
    number: '',
    // 图片地址演示使用
    // 正面身份证
    idcardFrontUrl: '',
    // 反面身份证
    idcardBackUrl: '',
  },
  // 获取要修改的id
  onLoad({ id }) {
    console.log(id, "ev");
    if (id) {
      wx.setNavigationBarTitle({
        title: '修改房屋内容'
      });
      this.evId = id
      // 数据回显
      this.getinfo()
    }
  },
  // 数据回显
  async getinfo() {
    const { data: obj } = await wx.http({
      url: '/room/' + this.evId
    })
    console.log(obj, '数据回显');
    this.setData({
      ...obj,
      number: obj.mobile,
      six: obj.gender
    })
  },
  // -----提交校验
  async goList() {
    const { id, point, building, room, name, six: gender, number: mobile, idcardFrontUrl, idcardBackUrl } = this.data
    const parameter = { id, point, building, room, name, gender, mobile, idcardFrontUrl, idcardBackUrl }
    // 1.验证姓名
    if (!/^[\u4e00-\u9fa5]{2,5}$/.test(name)) return wx.utils.toast('请填写中文姓名!')
    // 2.验证手机号
    if (!/^[1][3-8][0-9]{9}$/.test(mobile)) return wx.utils.toast('请填写正确的手机号码!')
    // 3. 图片地址不能为空
    if (!idcardBackUrl || !idcardFrontUrl) return wx.utils.toast('请上传身份证照片!')
    console.log('校验通过')
    // 请求后台上传数据
    console.log(point, building, room, name, gender, mobile, 'point, building, room, name, gender, mobile');
    // ___________________________注：修改也用的这个按钮接口，根据data是否有id来判别是添加还是修改
    if (id) {
      // 如果data变量里有id，就在请求参数中加上id
      parameter.id = id
    }
    try {

      const res = await wx.http.post('/room', parameter)
      // -------------------------?????有问题
      // const res = await wx.http({
      //   url: '/room',
      //   method: 'post',
      //   body: {
      //     point, building, room, name, gender, mobile
      //   }
      // })
      console.log(res, '上传结果');
      // 提示上传成功，但是后面跳转页面会覆盖，用定时器写
      setTimeout(() => {
        wx.utils.toast('数据上传成功', 'success')
      }, 2000)
      // 验证通过，跳转下一页面，reLaunch会销毁之前打开的所有页面
      wx.reLaunch({
        url: '/house_pkg/pages/list/index',
      })

    }
    catch (error) {
      console.log(error, '上传信息错误');
    }

  },
  // ----上传照片，这里的上传照片和上传头像那不一样，不用使用button标签
  addImg(ev) {
    // 1.获得临时地址chooseMedia支持async,await，这儿没有使用
    wx.chooseMedia({
      // 上传数量
      count: 1,
      // 上传类型
      emdiaType: ['image'],
      // 默认为压缩模式
      sizeType: ['compressed'],
      success: (res) => {
        console.log(res.tempFiles[0].tempFilePath, 'res');
        // 临时地址
        const url = res.tempFiles[0].tempFilePath
        // 2.上传获得长期地址
        // 这里回调黑洞了
        wx.uploadFile({
          url: wx.http.baseURL + '/upload',
          // 要上传的本地图片路径
          filePath: url,
          name: 'file',
          // 带请求头
          header: {
            Authorization: getApp().token
          },
          success: ({ data }) => {
            // 获取的data是字符串转换为对象，还原
            const dataRes = JSON.parse(data)
            // 如果请求时请求内容错误
            if (dataRes.code === 401) {
              wx.utils.toast('请求错误', 'error')
              return
            }
            // 上传成功
            if (dataRes.code === 1e4) {
              // 上传请求返回的网络地址改到渲染图片上
              console.log(dataRes.data.url, '长期地址');
              // 3.判断是在正面上传还是背面上传
              const type = ev.mark.type
              this.setData({
                // [变量不确定的]
                [type]: dataRes.data.url
              })
            }
          }
        })
      }
    })
  },
  // -----删照片
  removePicture(ev) {
    // 移除图片的类型（身份证正面或反面）
    console.log(ev.mark.type);
    const type = ev.mark.type
    this.setData({
      [type]: ""
    })
  },
})
