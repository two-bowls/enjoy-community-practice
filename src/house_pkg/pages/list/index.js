Page({
  data: {
    //渲染的数据列表
    list: []
  },
  onLoad() {
    this.getHouseList()
  },
  // 获取数据
  async getHouseList() {
    const { data } = await wx.http({
      url: '/room'
    }
    )
    console.log(data);
    this.setData({
      list: data
    })
  },
  // 滑块删除
  deleteHouse(ev) {
    // 这里的ev事件传参里面有mark传入的值，和有赞滑块传入的值
    console.log(ev.detail);
    const id = ev.mark.id
    const { position, instance } = ev.detail
    // 如果向右滑动并点击了删除
    if (position === "right") {
      // 如果点击了删除，弹窗提示是否确认删除
      wx.showModal({
        title: '确认删除吗',
        success: async (result) => {
          // result.confirm=true为点击删除
          if (result.confirm) {
            console.log("删除");
            // 请求后台删除数据
            await wx.http.delete('/room/' + id)
            this.getHouseList()
          }
          // result.cancel=true为点击取消
          if (result.cancel) {
            console.log("取消");
          }
        },

      });

    }
  },
  // 跳转页面，携带当前id，以便跳转到该页面用来请求数据
  goDetail({ mark: { id } }) {
    wx.navigateTo({
      url: '/house_pkg/pages/detail/index?id=' + id,
    })
  },

  addHouse() {
    wx.navigateTo({
      url: '/house_pkg/pages/locate/index',
    })
  },
})
