Page({
  //选中的那条数据，详情回填
  data: {
    list: ''
  },
  // 获取跳转时传入的id
  onLoad({ id }) {
    console.log(id);
    // 存到实例中，不进行页面渲染存到this实例中
    this.UrlId = id
    this.getList()
  },
  // 获取渲染数据
  async getList() {
    const { data: obj } = await wx.http.get('/room/' + this.UrlId)
    console.log(obj);
    this.setData({
      // 把获取的对象拆开存到data内
      ...obj
    })
  },
  // 点击修改，跳转页面（这个页面是和添加是同个页面），传入id用来区分是修改
  editHouse({ mark: { id } }) {
    wx.navigateTo({
      url: '/house_pkg/pages/form/index?id=' + id,
    })
  },
})
