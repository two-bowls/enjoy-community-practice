Page({
  data: {
    // 渲染数据条数
    size: '',
    // 渲染的数据每条的名字
    title: ''
  },
  // 获取路径的传参,传的参是每条数据的名字
  onLoad(ev) {
    // 伪造随机数，用于决定渲染数据几条内容,后台没有提供相应数据，自己伪造用于练习
    // Math.floor(),1往下的1位小数不包括1，Math.random()随机数
    const size = Math.floor(Math.random() * 10)
    console.log(size, ev);
    this.setData({
      size,
      title: ev.title
    })
  },
  // 点击跳转携带楼号
  go({ mark: { num } }) {
    console.log(num, '楼号');
    wx.navigateTo({
      url: "/house_pkg/pages/room/index?number=" + num + '号楼'
    });

  }
})