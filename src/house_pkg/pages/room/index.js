Page({
  data: {
    // 多少条数据
    size: '',
    rooms: [],
    point: '',
    building: ''
  },
  //point, building 小区名，楼层
  onLoad({ point, building }) {
    // 伪造房间数据
    // this.fake(point, building)
  },
  // 伪造房间号
  fack(point, building) {
    const rooms = []
    // 随机循环遍历次数
    const size = Math.floor(Math.random() * 5) + 4
    this.setData({
      size: size
    })
    console.log(size);
    for (let i = 0; i < size; i++) {
      // 1.每循环1次就随机生成一次楼层数
      const floor = Math.floor(Math.random() * 20) + 1
      // 2.随机房间号
      const No = Math.floor(Math.random() * 3) + 1
      console.log(No, 'no');
      // 3.把每次循环出来的结果组成数组，并将数组转换为一个字符串
      // 楼号0房间号
      const room = [floor, 0, No].join('')
      // 4.如果数据重复在数组里，继续循环不添加到数组中
      if (rooms.includes(room)) continue
      // 4.结果添加到数组中
      rooms.push(room)
    }
    // 更新数据，重新渲染
    this.setData({ rooms, point, building })
    console.log(rooms);
  }
})