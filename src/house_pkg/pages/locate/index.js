// 导入定位的js文件
// 获取默认位置经纬度的步骤：配置app.json文件，申明两个方法，和调用前询问是否同意获取当前位置信息
// 经纬度逆解析：
//1.下载腾讯位置的包，解压把里面的min.js文件复制到src里面创建libs文件夹放里面
//2.配置app.json文件，申明两个方法，和调用前询问是否同意获取当前位置信息
//3.简单封装逆解析方法，新建utils-->qqMap文件，文件内导入min.js,在默认导出实例化的对象，实例化中传入对象腾讯位置的key
// 4.使用时在该页面的js页面导入，在导入的实例化对象的原型上有reverseGeocoder(获取默认的位置)方法，方法使用查看腾讯位置服务官网
import QQMapWX from '../../../utils/qqMap'

Page({
  data: {
    position: '',
    keyPosition: []

  },
  // 获取默认位置
  async onLoad() {
    // wx.getPosition()结果为当前的地址，支持promise，可以用await,async
    const { latitude, longitude } = await wx.getLocation()
    console.log(latitude, longitude, '默认位置');
    this.getQqMap(latitude, longitude)
  },
  // 逆解析
  getQqMap(w, j) {
    QQMapWX.reverseGeocoder({
      location: {
        latitude: w,
        longitude: j
      },
      success: ({ result: { address } }) => {
        console.log(address);
        this.setData({
          position: address
        })
      }
    })
    // 逆解析完，关键词下表单渲染
    QQMapWX.search({
      keyword: '理发店',  //搜索关键词
      location: {
        latitude: w,
        longitude: j
      },
      page_size: 7,
      success: ({ data }) => {
        console.log(data, '关键字');
        this.setData({
          keyPosition: data
        })
      }
    })
  },
  // 点击获取位置
  async getPosition() {
    // res选择的位置经纬度chooseLocation方法支持promise
    const { latitude, longitude } = await wx.chooseLocation()
    console.log(latitude, longitude);
    // 逆解析经纬度
    this.getQqMap(latitude, longitude)
  },
  // 跳转页面
  routeUrl(ev) {
    console.log(ev.mark, 'ev');
    wx.navigateTo({
      // 跳转携带单条数据内容
      url: '/house_pkg/pages/building/index?title=' + ev.mark.name
    });

  }
})