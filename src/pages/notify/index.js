Page({
  data: {
    info: {}
  },
  onLoad(ev) {
    // 如果跳转页面没有携带参数
    if (!ev.id) return
    console.log(ev.id, 'ev');
    this.getInfo(ev.id)
  },
  // -----封装获取单个数据详情
  async getInfo(url) {
    const { data } = await wx.http({
      url: `/announcement/${url}`
    })
    console.log(data);
    this.setData({
      info: { ...data }
    })
    console.log(this.data.info, 'info');
  }
})