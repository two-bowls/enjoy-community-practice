// ----思路：输入手机号点击获取验证码，倒计时出现，倒计时出现不能重新点击获取验证码，输入验证码请求接口看是否输入正确，并存入调用app.js的方法存token
import minix from '../../utils/minix'
Page({
  data: {
    // 手机号
    number: '18437249124',
    // 校验结果
    checkRes: '',
    // 验证码按钮是否出现
    isShow: true,
    // 获取的验证码
    codea: '',
    evUrl: ''
  },
  // 混入对象，可以用混入的变量和方法,固定写法为数组
  behaviors: [minix],
  onLoad(ev) {
    this.check()
    this.setData({
      evUrl: ev.url
    })
  },
  // 校验输入的手机号符合不符合
  check() {
    const res = /^1(3\d|4[5-9]|5[0-35-9]|6[567]|7[0-8]|8\d|9[0-35-9])\d{8}$/.test(this.data.number.trim())
    this.setData({
      checkRes: res
    })

  },
  // 点击获取验证码事件
  async getCode() {
    const { checkRes } = this.data
    // 手机号校验不正确提示
    if (!checkRes) {
      wx.utils.toast('手机号不正确', 'error')
      return
    }
    // 点击验证码消失出现倒计时
    this.setData({
      isShow: !this.data.isShow
    })
    // 请求后台获取验证码
    const { code, data } = await wx.http({
      url: '/code',
      data: {
        mobile: this.data.number.trim()
      }
    })
    const numCode = data.code
    console.log(data.code, '获取验证码');
    if (code !== 1e4) {
      wx.utils.toast('验证码发送失败，请稍后', 'error')
      return
    }
    wx.utils.toast('验证码发送请查收', 'success')
    // 复制验证码
    wx.setClipboardData({
      data: numCode
    });

  },
  // 登录
  async login() {
    // 验证表单数据
    const res = /^\d{6}$/.test(this.data.codea)
    if (!this.data.checkRes) return wx.utils.toast('手机号不正确', 'error')
    if (!res) return wx.utils.toast('验证码不正确', 'error')
    // 请求接口
    const result = await wx.http({
      url: '/login',
      method: 'post',
      data: {
        mobile: this.data.number,
        code: this.data.codea
      }
    })
    console.log(result);
    // 获取到token存token
    if (result.code === 1e4) {
      console.log('asas');
      let app = getApp();
      // 1.存token
      app.setToken(result.data.token)
      // 2.有参数路径跳转参数路径无参数路径跳转首页
      const { evUrl } = this.data
      if (evUrl) {
        // 用混入的方法判断是否跳转的是tabbar页面
        if (this.isTabbar(evUrl)) {
          wx.switchTab({
            url: evUrl
          });
        }
        else {
          console.log(333);
          wx.redirectTo({
            url: evUrl,
          });
        }
      }
      else {
        wx.switchTab({
          url: "/pages/index/index"
        });

      }
    }
  },
  // 倒计时结束时
  countDownChange() {
    this.setData({
      isShow: true
    })
  }
})
