Page({
  data: {
    list: {
      imgUrl: '',
      nickName: ''
    }
  },
  // tabbar页面切换其他页面不会销毁，如果想每次打开刷新用onShow(每次展示时执行)
  onShow() {
    const res = wx.getStorageSync('用户信息');
    this.setData({
      list: JSON.parse(JSON.stringify(res))
    })
  },
  goLogin() {
    wx.navigateTo({
      url: '/pages/login/index',
    })
  },
})
