
Page({
  data: {
    listData: {
      imgUrl: '',
      nickName: ''
    }
  },
  // 每次打开页面初始化渲染上次本地已存的数据
  onLoad() {
    wx.getStorage({
      key: '用户信息',
      success: (result) => {
        console.log(result.data, 'result');
        this.setData({
          // 深拷贝转化为JSON字符串再还原
          listData: JSON.parse(JSON.stringify(result.data))
        })
      },
      fail: () => { },
      complete: () => { }
    });


  },
  getImg(e) {
    console.log(e.detail.avatarUrl, '临时图片地址');
    console.log(wx.http.baseURL + '/upload', '请求地址');
    console.log(getApp().token, 'token');
    this.getUrl(e.detail.avatarUrl)

  },
  // 封装上传头像// 选择的临时路径上传到后台，转换为长期路径，请求接口
  getUrl(localUrl) {
    // 上传图片到服务器用的另外的设置
    wx.uploadFile({
      url: wx.http.baseURL + '/upload',
      // 要上传的本地图片路径
      filePath: localUrl,
      name: 'file',
      // 带请求头
      header: {
        Authorization: getApp().token
      },
      success: ({ data }) => {
        // 获取的data是字符串转换为对象，还原
        const dataRes = JSON.parse(data)
        console.log(data, 'res');
        console.log(dataRes, '1234');
        // 如果请求时请求内容错误
        if (dataRes.code === 401) {
          wx.utils.toast('请求错误', 'error')
          return
        }
        // 上传成功
        if (dataRes.code === 1e4) {
          // 上传请求返回的网络地址改到渲染图片上
          this.setData({
            "listData.imgUrl": dataRes.data.url
          })
        }
        // 存到本地
        wx.setStorageSync("用户信息", this.data.listData);
      }
    })
  },
  // 昵称
  getnickName(ev) {
    console.log(ev.detail.value, 123);
    // 上传昵称到后台
    this.putUserName(ev.detail.value)


  },
  // 封装上传昵称
  async putUserName(a) {
    console.log(a, 'a');
    const { nickName } = this.data.listData
    // await wx.http.put('/userInfo', { nickName })
    try {
      const res = await wx.http.put('/userInfo', { nickName: nickName })
      console.log(res.message, '昵称');
      wx.utils.toast(res.message, 'success')
      // 上传成功，把选中的昵称赋值给data变量里，用来存到本地
      this.setData({
        "listData.nickName": a
      })
      wx.setStorageSync("用户信息", this.data.listData);
    }
    catch (error) {
      wx.utils.toast('请稍后再试', 'error')
    }
    // this.setData({
    //   "listData.imgUrl": dataRes.data.url
    // })
    // console.log(123);
    // 存到本地

  }
})