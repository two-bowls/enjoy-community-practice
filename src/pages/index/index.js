Page({
  data: {
    list: []
  },
  onLoad() {
    wx.utils.toast('没有文字', 'error')
    this.getList()
  },
  //------封装获取列表信息
  async getList() {
    const { data } = await wx.http({
      url: '/announcement',
    })
    // console.log(data);
    // 深拷贝
    this.setData({
      list: JSON.parse(JSON.stringify(data))
    })
    // console.log(this.data.list, 'list');
  }
  //时间格式进行封装

})
